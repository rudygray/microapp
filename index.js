const express = require('express');
const bodyParser = require('body-parser');
const favicon = require('serve-favicon');

const app = express();
const port = process.env.PORT || 3208;
app.listen(port);

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(favicon(path.join(__dirname, 'favicon.ico')));

app.get('/', function(req, res){
  res.status(200).send('Access granted: ');
});

console.log('API server started on: localhost:' + port);
